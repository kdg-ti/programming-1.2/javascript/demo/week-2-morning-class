/*
// Higher-order function
function handleAll(array, someFunction) { // 2nd argument is 'f'
    for (let i = 0; i < array.length; i++) {
        const elementU = array[i];
        someFunction(elementU); // f is called with ONE arg.
    }
}

const a = [1,2,3];
function f(n) {
    console.log("Number: " + n);
}
handleAll(a, f);
handleAll(a, console.log);
*/

const x = 7;
const a = ['ABC', "test", `X=${x}`];
a.forEach(f);

function f(string, index, entireArray) {
    console.log('STRING: ' + string + ' (index ' + index + ')');
}

// a.forEach(console.error);

a.forEach(element => console.log('element: ' + element));

// Predicate
const filteredArray = a.filter((element, index) => {
    //return element === element.toUpperCase();
    //return index % 2 === 0;
    return element.charAt(0) === 't';
});
console.log(filteredArray);

// 'map' --> maps elements (not to be confused with the 'Map' data type)
const mappedArray = a.map(element => element.toLowerCase());
console.log(mappedArray);
const numbers = [1.2, 7.5, 63.9];
console.log(numbers.map(n => Math.round(n)));

// Reduce --> reduce an array into a single value
const singleValue = a.reduce((previous, current) => {
    return previous + " --- " + current;
}/*, ""*/);
console.log(singleValue);

// First, my lambda will be called with element ZERO and element ONE
// Next, my lambda will be called with the VALUE so far and element TWO

const theSum = numbers.reduce((acc, value) =>
        acc + value);
console.log('theSum: ' + theSum);
