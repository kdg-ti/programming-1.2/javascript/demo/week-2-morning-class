// CLOSURE
/*
const n = 42;

function f(a, b, c) {
    return a + b + c + n; // Closure
}

const result = f(1, 2, 3);
console.log('Result: ' + result);

// DESTRUCTURING
const obj = {
    name: 'Lars',
    age: 40
};

function printObject(o) {
    console.log(`Object: ${o}`);
}

printObject(obj);

//const name = obj.name;
//const age = obj.age;
const {name, age} = obj; // Destructuring
console.log(`Name: ${name}, Age: ${age}`);

const arr = [45, 92, 12];
console.log(`First element: ${arr[0]}, Second element: ${arr[1]}`);
const [first, second] = arr;
console.log(`First element: ${first}, Second element: ${second}`);

f2(arr);

function f22(array) {
    const abc = array[0];
    const ghi = array[2];
}

function f2([abc, , ghi]) {
    console.log(`First element: ${abc}, Third element: ${ghi}`);
}

f3(obj, 25.6);

function f3({name, age}, number) {
    console.log(name + age + number);
}
*/
// JSON --> serialization
const person = {
    name: 'Lars',
    age: 40.2,
    happy: true,
    course: {
        courseName: 'Programming 1.2'
    }
};
// <person> <name>Lars</name> <age>40</age>  </person>  XML
// { "name": "Lars", "age": 40 }                        JSON

console.log(JSON.stringify(person));
console.log(JSON.stringify([1, 2, 3]));
console.log(JSON.stringify({id: 7, children: [1, {name: "Jack"}]}));

const o1 = JSON.parse('{"test": false, "array": ["3", 4, {"a": true}]}');
console.log(o1.array[1]);
console.log(o1.array[2].a);
