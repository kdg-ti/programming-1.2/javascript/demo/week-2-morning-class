class Computer {
    static RAM_UNIT = 'GB';

    constructor(cpuSpeed, amountOfRam) { // NOT 'Computer'
        this._cpuSpeed = cpuSpeed;
        this._amountOfRam = amountOfRam;
    }

    get cpuSpeed() {
        return this._cpuSpeed;
    }

    set cpuSpeed(cpuSpeed) {
        /*if (cpuSpeed < 0) {
            throw new Error('has to be greater than zero!');
        }*/
        this._cpuSpeed = cpuSpeed;
    }

    fancyPrint() {
        console.log("COMPUTER");
        console.log("========");
        this.print();
    }

    print() { // NOT function
        console.log(`CPU: ${this._cpuSpeed}; RAM: ${this._amountOfRam}${Computer.RAM_UNIT}`);
    }
}

const c1 = new Computer(3.0, 8);
c1.fancyPrint();
c1.cpuSpeed = 2.5; // CALLING A SETTER!!!
c1.fancyPrint();

console.log("SPEED: " + c1.cpuSpeed); // CALLING A GETTER!!!

class Laptop extends Computer {
    constructor(cpuSpeed, amountOfRam, touchScreen) {
        super(cpuSpeed, amountOfRam);
        this._touchScreen = touchScreen;
    }

    print() {
        super.print();
        console.log('Touch screen? ' + (this._touchScreen ? 'yes' : 'no'));
    }
}

const l1 = new Laptop(2.8, 4, true);
l1.fancyPrint();

console.log(c1 instanceof Computer);
console.log(c1 instanceof Laptop);
console.log(l1 instanceof Computer);
console.log(l1 instanceof Laptop);

const m = new Map();
m.set("Lars", 40);
m.set("Someone else", 41);

const age = m.get("Lars");
console.log("AGE: " + age);

// NOT A MAP
const obj = {};
obj["Lars"] = 40;
obj["Someone else"] = 41;
